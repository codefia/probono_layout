 // document ready
 $(document).ready(function(){
        slider();
});

// menu trigger

$(function(){

    var $burger = $('.burger'),
        $menuClose = $('.menu_close'),
        $mobileMenu = $('.menu_nav_mobile'),
        $body = $('body');

    $burger.on('click', function(e){
        e.preventDefault();
        $mobileMenu.addClass('active');
        $body.addClass('flowhidden');
    });

    $menuClose.on('click', function(e){
        e.preventDefault();
        $mobileMenu.removeClass('active');
        $body.removeClass('flowhidden');
    });


})


function slider(){
    $('.control-dot').first().addClass('active');
    $('.slider-unit').first().addClass('active');
    $('.control-dot').click(function(){
        var $this = $(this),
            $dotLength = $this.parent().children(),
            position = $dotLength.index($this);
        $('.control-dot').removeClass('active').eq(position).addClass('active');
        $('.slider-unit').removeClass('active').eq(position).addClass('active');
    });
};

 //validate contact form on integrator bankowy
 $(function() {
     $('#form_banking_integrator form').validate({
         rules: {
             name: {
                 required: true,
                 minlength: 3
             },
             phone: {
                 required: true,
                 minlength: 9
             }
         },
         messages: {
             name: {
                 required: "Proszę o podanie imienia i nazwiska?",
                 minlength: "Twoje imię i nazwisko powinno posiadać przynajmniej 3 znaki"
             },
             phone: {
                 required: "Aktualny numer telefonu do kontaktu",
                 minlength: "Numer telefonu składa się z przynajmniej 9 cyfr"
             }
         },
         submitHandler: function(form) {

             $(form).ajaxSubmit({
                 type:"POST",
                 data: $(form).serialize(),
                 url:"php/contact_form.php",
                 success: function() {
                     $('#form_banking_integrator form :input').attr('disabled', 'disabled');
                     $('#form_banking_integrator form').fadeTo( "slow", 0, function() {
                         $(this).find(':input').attr('disabled', 'disabled');
                         $(this).find('label').css('cursor','default');
                         $('#success').fadeIn();
                     });
                 },
                 error: function() {
                     $('#form_banking_integrator form').fadeTo( "slow", 0.15, function() {
                         $('#error').fadeIn();
                     });
                 }
             });
         }
     });
 });

 //validate contact form in contact page
 $(function() {
     $('#contact_form form').validate({
         rules: {
             description: {
                 required: true,
                 minlength: 20
             },
             email: {
                 required: true,
                 email: true
             },
             message: {
                 required: true,
                 minlength: 100
             },
             phone: {
                 required: true,
                 minlength: 9
             },
             consent: {
                 required: true
             }
         },
         messages: {
             description: {
                 required: "Proszę o podanie tematu wiadomości",
                 minlength: "Temat wiadomości powinnien posiadać długośc min. 20 znaków"
             },
             email: {
                 required: "Proszę o podanie adresu email"
             },
             message: {
                 required: "Proszę o wpisanie treści wiadomości"
             },
             phone: {
                 required: "Aktualny numer telefonu do kontaktu",
                 minlength: "Numer telefonu składa się z przynajmniej 9 cyfr"
             },
             consent: {
                 required: "Musisz wyrazić zgodę na naszą politykę prywatności"
             }
         },
         submitHandler: function(form) {

             $(form).ajaxSubmit({
                 type:"POST",
                 data: $(form).serialize(),
                 url:"php/main_contact_form.php",
                 success: function() {
                     $('#contact_form form :input').attr('disabled', 'disabled');
                     $('#contact_form form').fadeTo( "slow", 0, function() {
                         $(this).find(':input').attr('disabled', 'disabled');
                         $(this).find('label').css('cursor','default');
                         $(this).css("display", "none");
                         $('#success').fadeIn();
                     });
                 },
                 error: function() {
                     $('#contact_form form').fadeTo( "slow", 0.15, function() {
                         $('#error').fadeIn();
                     });
                 }
             });
         }
     });
 });

// extend validation messages

 $.extend(jQuery.validator.messages, {
     required: "To pole jest wymagane.",
     remote: "Proszę poprawić to pole.",
     email: "Proszę podać prawidłowy adres email.",
     url: "Proszę o podanie prawidłowego adresu URL.",
     date: "Proszę o podanie poprawnej daty.",
     dateISO: "Proszę o podanie poprawnej daty (ISO).",
     number: "Proszę o podanie poprawnego numeru.",
     phone: "Proszę o podanie poprawnego numeru.",
     digits: "Proszę o wpisanie tylko cyfr.",
     creditcard: "Proszę o podanie prawidłowego numeru karty kredytowej.",
     equalTo: "Proszę o podanie tej samej wartości ponownie.",
     accept: "Proszę o podanie wartości z prawidłowym rozszerzeniem.",
     maxlength: jQuery.validator.format("Proszę o podanie nie więcej niż {0} znaków."),
     minlength: jQuery.validator.format("Proszę o wpisanie {0} znaków."),
     rangelength: jQuery.validator.format("Proszę o podanie wartości w przedziale {0} i {1} długości znaków."),
     range: jQuery.validator.format("Proszę o podanie wartości pomiędzy {0} i {1}."),
     max: jQuery.validator.format("Proszę o podanie wartości mniejszej lub równej {0}."),
     min: jQuery.validator.format("Proszę o podanie wartości większej lub równej {0}.")
 });